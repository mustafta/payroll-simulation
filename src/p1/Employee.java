/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package p1;

/**
 *@author mustafta
 * Employee parent Class 
 */
    public class Employee { 
    //identify  parameteres 
                 
    private String name; 
    private int number_of_hours;
    private double hourlyWage;
    
    //constructor with no args
    public Employee (){}
    
    //constructor with args
    public Employee (String name, int number_of_hours, double hourlyWage)
    {
    this.name = name;
    this.number_of_hours = number_of_hours;
    this.hourlyWage = hourlyWage;
    }
    
    //set method for name
    public void setName(String name) {
        this.name = name;
    }
    
    //get method for name
    public String getName() {
        return name;
    }
    
    //set method for number of hours
    public void setNumber_of_hours(int number_of_hours) {
        this.number_of_hours = number_of_hours;
    }
 
    //get method for number of hours
    public int getNumber_of_hours() {
        return number_of_hours;
    }
    
     public void hourlyWage(double hourlyWage) {
        this.hourlyWage = hourlyWage;
    }
 
    
    public double getHourlyWage() {
        return hourlyWage;
    }
    
    public double calculatePay() {
        return number_of_hours*hourlyWage;
                           }

    
    //toString Method
    @Override
    public String toString() {
        return  "Employee Name " + name + "\t" + "Paycheque " + calculatePay();
    }
    
    }
