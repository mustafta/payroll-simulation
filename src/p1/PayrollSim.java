/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package p1;

public class PayrollSim {
    public static void main(String[] args) {
        EmployeeFactory factory= EmployeeFactory.getInstance();
        Employee emp1 = factory.getEmployee (EmployeeType.EMPLOYEE, "Sara Wilson",20, 150, 0 );
        Manager emp2= new Manager ("John Smith",40, 200, 10);
        System.out.println(emp1.toString());
        System.out.println(emp2.toString());
    }
}

    

