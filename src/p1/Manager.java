/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package p1;



/**
 *
 * @author Taghreed
 */
    public class Manager extends Employee {
    
    private double bonus;
    
    
    //constructor with no args
    public Manager (){
    super();
    }
    
    //constructor with args
    public Manager ( String name, int number_of_hours, double hourlyWage, double bonus)
    {
    super(name, number_of_hours, hourlyWage);
    this.bonus = bonus;
    }
    
    //set method for bonus
   public void setBonus(double bonus) {
   this.bonus = bonus;
   }
    
    //get method for bonus
    public double getBonus() {
        return bonus;
    }
    
    public double calculatePay() {
	return super.calculatePay()+bonus;
} 
    
    public String toString() {
        return super.toString() ;
}
    }
