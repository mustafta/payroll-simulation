
package p1;
public class EmployeeFactory {
     private static EmployeeFactory EmployeeFact;
    private EmployeeFactory(){
      
    }
    
    public static EmployeeFactory getInstance(){
        
        if (EmployeeFact == null){
        EmployeeFact = new EmployeeFactory();
    }
    return EmployeeFact; 
}
    
    public Employee getEmployee(EmployeeType type, String name, int number_of_hours, double hourlyWage, double bonus){
    Employee employees =null;
        switch (type)
    {
        case EMPLOYEE: 
            employees=  new Employee (name, number_of_hours, hourlyWage);
            break;
        case MANAGER:
            employees =  new Manager(name, number_of_hours, hourlyWage, bonus);  
            break;
    
    }
    return employees ;
    }
}

